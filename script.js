/*
Теоретичні питання.
1. Методи "setTimeout" та "setInterval" дозволяють виконать функцію з якоюсь часовою витримкою. Тобто планувати виклик
функції. Але "setTimeout" дозволяє запускати код через певний проміжок часу. А "setInterval" дозволяє виконати функцію
багаторазово за окремий проміжок часу.
2. Фактична затримка між викликами "func" для "setInterval" менша, ніж у коді, тому що проміжок часу, необхідний для виконання
"func" забирає частину вказаного інтервалу. А от при використанні вкладеного "setTimeout" буде гарантована затримка, вказана у коді,
тому що новий виклик функції планується по завершенні попереднього. У більшості браузерів, включаючи Chrome та Firefox,
 внутрішній лічильник продовжує цокати під час показу alert/confirm/prompt.
3. Для припинення виконання функції, викликаною методами "setTimeout" або "setInterval" треба використовувати clearInterval(),
clearTimeout(). Наприклад: let timerId = setTimeout(...); clearTimeout(timerId).
*/
'use strict'
const btn = document.getElementById('btn');
const div = document.getElementById('text');

btn.addEventListener('click', function (e){
    const changeBtn = document.querySelector('.change');

        if(e.target === changeBtn){
            function slowdown (cont){
                div.textContent = cont;
                btn.classList.toggle('change');
            }
                setTimeout(slowdown, 3000, 'Операція виконана');

        } else{
            div.textContent = 'Коли ви натиснете на кнопку, текст буде змінено через 3 секунди';
            btn.classList.toggle('change');
        }
    });

function timer(){
    let start = 10;
    const newEl = document.createElement('p');
    newEl.id = 'timerP';
    div.append(newEl);
    const par = document.getElementById('timerP');
    par.style.cssText = `display: block; margin: 100px auto; font-size: 80px; font-weight: 700; color: blue`;
    par.textContent = start;
    const int = setInterval(() => {
        start--;
        par.textContent = start;
        if(start < 1){
            clearInterval(int);
            par.textContent = 'Зворотній відлік завершено';

        }
    }, 1000);
}
document.addEventListener('DOMContentLoaded', timer);




















